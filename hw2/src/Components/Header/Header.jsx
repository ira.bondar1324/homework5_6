import React, {useContext} from "react";
import styles from "./Header.module.scss";
import PropTypes from "prop-types";
import {NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import {ReactComponent as GridIcon} from "../../img/grid_icon (2).svg";
import {ReactComponent as ListIcon} from "../../img/list_icon (2).svg";
import ThemContext from "../../context/ThemeContext.js/ThemeContext";

const Header=()=>{

const countFavourite=useSelector(state=>state.favourite.countFavourite);
const countCart=useSelector(state=>state.cart.countCart);

const {theme, toggleTheme}=useContext(ThemContext);

return(
    <>
    <header className={styles.headerWrapper}>  

        <NavLink to='/'>
            <img src="https://timeshop.com.ua/images/ab__webp/logos/169/timeshop-logo-1_png.webp" alt="Timeshop" />
        </NavLink>
        <ul className={styles.headerWrapperIcons}>

            <li className={styles.headerWrapperItem}>
                <NavLink to="/favorite" className={styles.headerWrapperIconsItem}>
                    <p className={styles.headerIconText}>Favorite</p>
                    <p className={styles.headerIconText}>{countFavourite}</p>
                </NavLink>
            </li>

            <li className={styles.headerWrapperItem}>
                <NavLink to="/cart" className={styles.headerWrapperIconsItem}>
                    <p className={styles.headerIconText}>Cart</p>
                    <p className={styles.headerIconText}>{countCart}</p>
                </NavLink>
            </li>
        </ul>

        <div className={styles.headerWrapperButtons}>
            <button onClick={()=>{toggleTheme("grid")}} className={styles.headerButton}><GridIcon/></button>
            <button onClick={()=>{toggleTheme("list")}} className={styles.headerButton}><ListIcon/></button>
        </div>
    </header>

    </>
    )
}

Header.propTypes={
    countFavorite:PropTypes.number,
    countCart:PropTypes.number
}

Header.defaultProps={
    countFavorite:0,
    countCart:0
}

export default Header;