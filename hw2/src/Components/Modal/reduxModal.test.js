import Modal from "./Modal";
import { render,screen,fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../redux/store";

const Component = (props) => {

    return (
        <Provider store={store}>
            <Modal />
        </Provider>
    )
}


describe('Smoke test (Snapshot test)', () => {
    test('should Modal render', () => {
        const { asFragment } = render(<Component />);
        expect(asFragment()).toMatchSnapshot();
    });
});


describe("Modal`s buttons test", () => {

    test('should click on button X close modal', () => {
        render(<Component />);
        const btn=screen.getByText("X")
        fireEvent.click(btn);
        expect(screen.queryByRole("button")).not.toBeInTheDocument();
    });

    test('should click on button No close modal', () => {
        render(<Component />);
        const btn=screen.getByText("No")
        fireEvent.click(btn);
        expect(screen.queryByRole("button")).not.toBeInTheDocument();
    });

    test('should click on button Yes close modal', () => {
        render(<Component />);
        const btn=screen.getByText("Yes")
        fireEvent.click(btn);
        expect(screen.queryByRole("button")).not.toBeInTheDocument();
    });
});