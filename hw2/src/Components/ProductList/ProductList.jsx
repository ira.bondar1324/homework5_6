import React,{useContext} from "react";
import ProductItem from "../ProductItem/ProductItem";
import styles from "./ProductList.module.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { shallowEqual } from "react-redux";
import { saveStateToLocalStorage } from "../../utils/localStorageHelper";
import ThemContext from "../../context/ThemeContext.js/ThemeContext";

const ProductList=({type})=>{
    const {theme, toggleTheme}=useContext(ThemContext);
    
    const items = useSelector((state)=>state.items.items,shallowEqual);
    saveStateToLocalStorage("items",items.products);
    const cart=useSelector((state)=>state.cart.cart);
    const favourite=useSelector((state)=>state.favourite.favourite);

return(
    <ul className={theme==="grid"?styles.productListGrid:styles.productList}>
            {
                type==="cart"?
                    cart.length!==0? cart.map(el=><ProductItem type={type} key={el.id} el={el}/>): <h3>Not found element in Cart</h3>
                :type==="home"?(
                    items.length!==0?
                    items.products.map(el=><ProductItem type={type} key={el.id} el={el}/>)
                    :<h3>Not found </h3>
                ):
                (favourite.length!==0?
                    favourite.map(el=><ProductItem type={type} key={el.id} el={el}/>)
                    :<h3>Not found element in Favourite</h3>
                )
            }
    </ul>
);
}

ProductList.propTypes={
    type:PropTypes.string,
}

ProductList.defaultProps={
    type:'',
}

export default ProductList;