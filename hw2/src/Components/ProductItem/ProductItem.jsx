import React,{useContext} from "react";
import styles from "./ProductItem.module.scss";
import Button from "../Button/Button.jsx";
import {ReactComponent as HeartIconPlus} from "../../img/heartIconRemove.svg";
import PropTypes from "prop-types";
import ThemContext from "../../context/ThemeContext.js/ThemeContext";

const ProductItem=({el,type})=>{
    const {theme, toggleTheme}=useContext(ThemContext);

return(
    <li className={theme==="grid"?styles.productItem:styles.productItemList}>
        <img className={theme==="grid"?styles.productItemImg:styles.productItemImgList} src={el.url} alt="" />
        <div className={theme==="grid"?styles.productItemInfo:styles.productItemInfoList}>
            <h3 className={styles.productItemName}>{el.name}</h3>
            <h4 className={styles.productItemColor}>Color: {el.color}</h4>
            <h4 className={styles.productItemPrice}>{el.price} $</h4>
            {type==="cart"?<h4 className={styles.productItemCount}>Count: {el.countInCart}</h4>:null}
        </div>
        <div className={theme==="grid"?styles.productItemBtns:styles.productItemBtnsList}>
            <Button type="favourite" el={el} 
            backgroundColor="white">
                <HeartIconPlus className={el.favorite?styles.heartBtnTrue:styles.heartBtnFalse}/>
            </Button>
            <Button type={type} el={el} backgroundColor="black">{type==="cart"? "Delete from cart":"Add to cart"}</Button>
        </div>
    </li>
);
}

ProductItem.propTypes={
    type:PropTypes.string,
    el:PropTypes.object,
}

ProductItem.defaultProps={
    type:"",
    el:{},
}

export default ProductItem;