import Button from "./Button";
import Modal from "../Modal/Modal";
import { render,screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "../../redux/store";


const Component = () => {

    return (
        <Provider store={store}>
            <Button>hello</Button>
        </Provider>
    )
}

describe('Smoke test (Snapshot test)', () => {
    test('should Button render', () => {
        const { asFragment } = render(<Component />);
        expect(asFragment()).toMatchSnapshot();
    });
});
