import React from "react";
import styles from "./Button.module.scss";
import PropTypes from "prop-types";
import { toggleModal,typeModal } from "../../redux/modal/actionCreators";
import { useDispatch } from "react-redux";
import {setElement} from "../../redux/items/actionCreators";
import { addFavourite,deleteFavourite } from "../../redux/favourites/actionCreators";

const Button = ({type,backgroundColor,el,children}) => {
    const dispatch=useDispatch();
    
    const helper=()=>{
        if(type==="home"){
            dispatch(toggleModal(true));
            dispatch(typeModal(type));
            dispatch(setElement(el));
        }else if(type==="cart"){
            dispatch(toggleModal(true));
            dispatch(typeModal(type));
            dispatch(setElement(el));
        }else{
            if(!el.favorite){
                dispatch(addFavourite(el));
            }else{
                dispatch(deleteFavourite(el));
            }
        }
    }
    return(
            <button onClick={helper}className={styles.button} style={{backgroundColor:backgroundColor}}>{children}</button>
    );
}

Button.propTypes={
    type:PropTypes.string,
    backgroundColor:PropTypes.string,
    el:PropTypes.object,
    children:PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
}

Button.defaultProps={
    type:'',
    backgroundColor:'',
    el:{},
    children:''
}

export default Button;