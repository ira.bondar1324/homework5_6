import React from "react";
import { useField } from "formik";
import styles from "./Input.module.scss";

const Input=(props)=>{

    const [field,meta]=useField(props.name);

    const {error,touched}=meta;

    return(
        <div className={styles.inputWrapper}>
            <input className={styles.input} type="text" {...field}  {...props}/>
            {touched && error && <p className={styles.error}>{error}</p>}
        </div>
    )

}
export default Input;