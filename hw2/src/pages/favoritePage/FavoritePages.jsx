import React from "react";
import ProductList from "../../Components/ProductList/ProductList";
import PropTypes from "prop-types";

const FavoritePages=()=>{

    return(
        <>
            <ProductList type="favourite"/>
        </>
    );
}


export default FavoritePages;