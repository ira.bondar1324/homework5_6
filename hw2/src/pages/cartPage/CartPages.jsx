import React from "react";
import ProductList from "../../Components/ProductList/ProductList";
import Modal from "../../Components/Modal/Modal";
import PropTypes from "prop-types";
import FormBuyProduct from "../../Components/forms/FormBuyProduct/FormBuyProduct";

const CartPages=()=>{

    return(
        <>
            <ProductList type='cart'/>
            <Modal/>
            <FormBuyProduct/>
        </>
    );
}

export default CartPages;