import { SET_ITEM,SET_ELEMENT} from "./actions";


const initialValue={
    items:[],
    element:{},
}


const itemsReducer = (state=initialValue,action)=>{
    switch(action?.type){
        case (SET_ITEM):{
            return {...state,items:action.payload}
        }
        case (SET_ELEMENT):{
            return {...state,element:action.payload}
        }
        default: return state;
    }
}

export default itemsReducer;