"use strict";

import { SET_ITEM, SET_ELEMENT} from "./actions";
import { saveStateToLocalStorage} from "../../utils/localStorageHelper";

export const setItems = (items)=>({type:SET_ITEM, payload:items});
export const setElement = (item)=>({type:SET_ELEMENT, payload:item});



export const fetchItems=()=>{
    return async(dispatch,getState)=>{
        try{
            const data=await fetch("./products.json").then(res=>res.json());
            dispatch(setItems(data));
            const elem =getState().items;
            saveStateToLocalStorage("items",JSON.stringify(elem))
        }catch(err){
            console.log(err.message);
        }
    }

}