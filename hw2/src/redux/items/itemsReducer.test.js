import itemsReducer from './reducer';
import { setItems, setElement} from "./actionCreators";


const initialValue={
    items:[],
    element:{},
}


describe("Items reducer tests", () => {

    test("should return default state", () => {
        expect(itemsReducer()).toEqual(initialValue)
    })

    test("should set new state items", () => {
        expect(itemsReducer(initialValue, setItems([{ name: "watch", color: "silver" }]))).toEqual({ "items": [{ "color": "silver", "name": "watch" }], element:{}})
    })

    test("should set new element in state items", () => {
        expect(itemsReducer(initialValue, setElement({ name: "watch", color: "silver" }))).toEqual({ "items": [], element:{ name: "watch", color: "silver" }})
    })

})