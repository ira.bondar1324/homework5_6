import modalReducer from './reducer';
import { toggleModal, typeModal} from "./actionCreators";


const initialValue={
    isOpen:false,
    type:null
}


describe("Modal reducer tests", () => {

    test("should return default state", () => {
        expect(modalReducer()).toEqual(initialValue)
    })

    test("should set new toggleModal", () => {
        expect(modalReducer(initialValue, toggleModal(true))).toEqual({isOpen:true, type:null})
    })

    test("should set new type modal", () => {
        expect(modalReducer(initialValue, typeModal("home"))).toEqual({isOpen:false, type:"home"})
    })
})