import cartReducer, { initialValue } from './reducer';
import { setCart, setCountCart, addCartElement, deleteCartElement } from "./actionCreators";

const newInitialValues =
{
    "cart":
        [
            { "cart": true, "color": "silver", "countInCart": 1, "name": "watch" }
        ],
        "countCart": 1
}

describe("Cart reducer tests", () => {

    test("should return default state", () => {
        expect(cartReducer()).toBe(initialValue)
    })

    test("should set new state cart", () => {
        expect(cartReducer(initialValue, setCart([{ name: "watch", color: "silver" }]))).toEqual({ "cart": [{ "color": "silver", "name": "watch" }], "countCart": 0 })
    })

    test("should set new state countCart", () => {
        expect(cartReducer(initialValue, setCountCart(3))).toEqual({ "cart": [], "countCart": 3 })
    })

    test("should add new item to countCart and cart", () => {
        expect(cartReducer(initialValue, addCartElement({ name: "watch", color: "silver", "countInCart": 0 }))).toEqual({ "cart": [{ "cart": true, "color": "silver", "countInCart": 1, "name": "watch" }], "countCart": 1 })
    })

    test("should delete element from countCart and cart", () => {
        expect(cartReducer(newInitialValues, deleteCartElement({ name: "watch", color: "silver", "countInCart": 1 }))).toEqual({ "cart": [], "countCart": 0 })
    })
})