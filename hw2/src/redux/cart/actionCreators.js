import {ADD_ELEMENT_CART,DELETE_ELEMENT_CART,SET_CART,SET_COUNT_CART} from "./actions";

export const addCartElement = (elem)=>({type:ADD_ELEMENT_CART, payload:elem});
export const deleteCartElement = (elem)=>({type:DELETE_ELEMENT_CART, payload:elem});
export const setCart = (cart) => ({ type: SET_CART, payload: cart });
export const setCountCart = (countCart) => ({ type: SET_COUNT_CART, payload: countCart });


export const fetchCart = () => {
    return async (dispatch, getState) => {
        try {
            const cart=localStorage.getItem("cart");

            if(cart){
                dispatch(setCart(cart))
            }else{
                const cartState = getState().cart;
                localStorage.setItem('cart', JSON.stringify(cartState))
            }
        } catch (err) {
            console.log(err.message);
        }
    }
}

export const addCart = (item) => {
    return async (dispatch, getState) => {
        try {
            dispatch(addCartElement(item));
            const cart = getState().cart;
            localStorage.setItem('cart', JSON.stringify(cart.cart))
        } catch (err) {
            console.log(err.message);
        }
    }
}

export const deleteCart = (item) => {
    return async (dispatch, getState) => {
        try {
            dispatch(deleteCartElement(item));
            const cart = getState().cart;
            localStorage.setItem('cart', JSON.stringify(cart.cart))
        } catch (err) {
            console.log(err.message);
        }
    }
}
