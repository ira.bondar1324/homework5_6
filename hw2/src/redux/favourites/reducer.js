import {ADD_ELEMENT_FAVOURITE,DELETE_ELEMENT_FAVOURITE,SET_FAVOURITE} from "./actions";
import { saveStateToLocalStorage,getStateFromLocalStorage } from "../../utils/localStorageHelper";

let initialValue={
    favourite:[],
    countFavourite:0,
}

if(getStateFromLocalStorage("favourites")&&getStateFromLocalStorage("countFavourites")){

    initialValue={
        favourite:getStateFromLocalStorage("favourites"),
        countFavourite:getStateFromLocalStorage("countFavourites")
    }
}

const favouriteReducer = (state=initialValue,action)=>{
    switch(action?.type){
        case SET_FAVOURITE:{
            return {...state, favourite: action.payload }
        }
        case (ADD_ELEMENT_FAVOURITE):{
            const newFav=[...state.favourite];
            let newCount=(state.countFavourite);
            const elem=action.payload;
            const index =newFav.findIndex((el)=>el.id===elem.id);
            if(index===-1){
                newFav.push(elem);
                newCount++;
                elem.favorite=true;
            }
            saveStateToLocalStorage("favourites",newFav)
            saveStateToLocalStorage("countFavourites",newCount)
            return {...state,favourite:newFav,countFavourite:newCount}
        }
        case (DELETE_ELEMENT_FAVOURITE):{
            const newFav=[...state.favourite];
            let newCount=(state.countFavourite);
            const elem=action.payload;
            const index=newFav.findIndex(el=>el.id===elem.id);
            elem.favorite=false;
            newCount--;
            newFav.splice(index,1);
            saveStateToLocalStorage("favourites",newFav)
            saveStateToLocalStorage("countFavourites",newCount)
            return {...state,favourite:newFav,countFavourite:newCount}
        }

        default: return state;
    }
}

export default favouriteReducer;