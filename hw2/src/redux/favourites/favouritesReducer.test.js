import favouriteReducer from './reducer';
import { setFavourire, addFavouriteElement, deleteFavouriteElement } from "./actionCreators";


let initialValue = {
    favourite: [],
    countFavourite: 0,
}

const newInitialValues =
{
    "favourite":
        [
            { "color": "silver", "name": "watch", "favorite": true }
        ],
         "countFavourite": 1
}

describe("Favourites reducer tests", () => {

    test("should return default state", () => {
        expect(favouriteReducer()).toEqual(initialValue)
    })

    test("should set new state favourites", () => {
        expect(favouriteReducer(initialValue, setFavourire([{ name: "watch", color: "silver" }]))).toEqual({ "favourite": [{ "color": "silver", "name": "watch" }], "countFavourite": 0 })
    })

    test("should add new item to countCart and cart", () => {
        expect(favouriteReducer(initialValue, addFavouriteElement({ name: "watch", color: "silver", "favorite": false, }))).toEqual({ "favourite": [{ "color": "silver", "name": "watch","favorite": true, }], "countFavourite": 1 })
    })

    test("should delete element from countCart and cart", () => {
        expect(favouriteReducer(newInitialValues, deleteFavouriteElement({ "color": "silver", "name": "watch", "favorite": true }))).toEqual({ "favourite": [], "countFavourite": 0 })
    })
})