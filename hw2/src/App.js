import './App.css';
import Header from './Components/Header/Header';
import React, { useEffect, useState } from 'react';
import AppRoutes from "./AppRoutes";
import { fetchItems, setItems } from './redux/items/actionCreators';
import { useDispatch, useSelector } from 'react-redux';
import ThemContext from './context/ThemeContext.js/ThemeContext';

function App() {
  const dispatch = useDispatch();
  const itemsState = useSelector((state) => state.favourite.favourite)

  const [theme,setTheme]=useState("grid");

  const toggleTheme=(theme)=>{
    setTheme(theme)
  }

  const value ={
    theme,
    toggleTheme
  }

  useEffect(() => {
    dispatch(fetchItems());
  }, [])


  return (
    <ThemContext.Provider value={value}>
      <Header />
      <AppRoutes />
    </ThemContext.Provider>

  );
}

export default App;
